
syntax enable
set termguicolors

let g:miramare_enable_italic = 1
let g:miramare_disable_italic_comment = 1

set tabstop=8
set softtabstop=0
set expandtab
set shiftwidth=4
set smarttab

set nu
set hlsearch

let g:haskell_enable_quantification = 1
let g:haskell_enable_recursivedo = 1
let g:haskell_enable_arrowsyntax = 1
let g:haskell_enable_pattern_synonyms = 1
let g:haskell_enable_typeroles = 1
let g:haskell_enable_static_pointers = 1
let g:haskell_backpack = 1

filetype plugin indent on

syntax on 

execute pathogen#infect()

