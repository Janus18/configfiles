#!/bin/sh

# Abort script when command exits with error
set -e

# Print each command before it is executed (only for debugging)
set -x


########################################################################
# Config
########################################################################

IPTABLES=$(which iptables)
IPTABLES_SAVE=$(which iptables-save)
IPTABLES_FILE="/etc/iptables/iptables.rules"

BIND_INTERFACE=""
LOCAL_NETWORK=""


########################################################################
# Initialize firewall
########################################################################

printf "Initializing firewall ...\n"
printf "Using bind interface: $BIND_INTERFACE \n"
printf "Using source network: $LOCAL_NETWORK \n"

# Flush all chains
$IPTABLES -t filter -F
$IPTABLES -t nat -F
$IPTABLES -t mangle -F
$IPTABLES -t raw -F

# Delete all user defined chains
$IPTABLES -t filter -X
$IPTABLES -t nat -X
$IPTABLES -t mangle -X
$IPTABLES -t raw -X

# Zero the packet and byte counters in all chains
$IPTABLES -t filter -Z
$IPTABLES -t nat -Z
$IPTABLES -t mangle -Z
$IPTABLES -t raw -Z

# Setup default chain policies (drop everything by default)
$IPTABLES -P INPUT DROP
$IPTABLES -P FORWARD DROP
$IPTABLES -P OUTPUT ACCEPT

printf "Done initializing!\n"

########################################################################
# Setup firewall rules
########################################################################

printf "Setting up firewall rules ...\n"


### Basic rules ###

# Accept incoming only from local
$IPTABLES -A INPUT -i $BIND_INTERFACE -s $LOCAL_NETWORK -j ACCEPT

# Accept established or related connections
$IPTABLES -A INPUT -i $BIND_INTERFACE -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

# Accept loopback traffic
$IPTABLES -A INPUT -i lo -j ACCEPT

# Accept ICMPv6 ND packets from local network
$IPTABLES -A INPUT -i $BIND_INTERFACE -p 41 -j ACCEPT

# Drop invalid packets
$IPTABLES -A INPUT -m conntrack --ctstate INVALID -j DROP

# Accept ping request
$IPTABLES -A INPUT -p icmp --icmp-type 8 -m conntrack --ctstate NEW -j ACCEPT

# Reject every other connection with adequate messages
$IPTABLES -A INPUT -p udp -j REJECT --reject-with icmp-port-unreachable
$IPTABLES -A INPUT -p tcp -j REJECT --reject-with tcp-reset
$IPTABLES -A INPUT -j REJECT --reject-with icmp-proto-unreachable

printf "Firewall is now configured\n"


########################################################################
# Display configuration (optional)
########################################################################

printf "Displaying current firewall configuration ...\n"
printf "\nContent of filter table:\n\n"
$IPTABLES -nvL -t filter
printf "\nContent of nat table:\n\n"
$IPTABLES -nvL -t nat
printf "\nContent of mangle table:\n\n"
$IPTABLES -nvL -t mangle
printf "\nContent of raw table:\n\n"
$IPTABLES -nvL -t raw
printf "\n"

printf "Persisting current iptables: $IPTABLES_FILE\n"
$IPTABLES_SAVE > $IPTABLES_FILE

exit 0

